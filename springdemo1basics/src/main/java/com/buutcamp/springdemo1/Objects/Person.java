package com.buutcamp.springdemo1.Objects;

public class Person {

    private String name;
    private int age;

    public Person() {
        System.out.println("Constructor init...");
    }

    public void init() throws Exception {
        System.out.println("Initiating person...");
    }
    public void destroy() throws Exception {
        System.out.println("Destroying person...");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
