package com.buutcamp.springdemo1.config;

import com.buutcamp.springdemo1.Objects.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class AppConfig {

    @Bean(name="personBeanId_1",initMethod = "init",destroyMethod = "destroy")
    @Scope("singleton")
    public Person person1() {
        return new Person();
    }

    @Bean(name="personBeanId_2",initMethod = "init",destroyMethod = "destroy")
    @Scope("singleton")
    public Person person2() {
        return new Person();
    }
}
