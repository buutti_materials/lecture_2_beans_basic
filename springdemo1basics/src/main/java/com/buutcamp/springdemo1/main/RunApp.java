package com.buutcamp.springdemo1.main;


import com.buutcamp.springdemo1.Objects.Person;
import com.buutcamp.springdemo1.config.AppConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class RunApp {

    public RunApp() {
        //do something
        ApplicationContext appCtx = new AnnotationConfigApplicationContext(AppConfig.class);

        //Beans can also be called using the class type, e.g., Person.class as argument
        //However, using IDs is best practice
        Person person = (Person) appCtx.getBean("personBeanId_1");

        person.setName("Matti");
        person.setAge(10);
        System.out.println("Person 1 bean name = " + person.getName());
        System.out.println("Person 1 bean age = " + person.getAge());

        person = (Person) appCtx.getBean("personBeanId_2");

        System.out.println("Person 2 bean name = " + person.getName());
        System.out.println("Person 2 bean age = " + person.getAge());

        ((AnnotationConfigApplicationContext) appCtx).close();

    }
}
